package com.example.customkeyboard;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.LinearLayout;

public class MyKeyboard extends LinearLayout implements View.OnClickListener {

    private Button button1, buttonEnter, button2, button3, button4, button5,
            button6, button7, button8, button9, button0, buttonDelete, btnSample;

    private SparseArray<String> keyValue = new SparseArray<>();
    private InputConnection inputConnection;

    public MyKeyboard(Context context) {
        this(context, null, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);

    }

    public void init(Context context, AttributeSet attrs) {

        LayoutInflater.from(context).inflate(R.layout.keyboard, this, true);

        button0 = (Button) findViewById(R.id.btn0);
        button0.setOnClickListener(this);
        button1 = (Button) findViewById(R.id.btn1);
        button1.setOnClickListener(this);
        button2 = (Button) findViewById(R.id.btn2);
        button2.setOnClickListener(this);
        button3 = (Button) findViewById(R.id.btn3);
        button3.setOnClickListener(this);
        button4 = (Button) findViewById(R.id.btn4);
        button4.setOnClickListener(this);
        button5 = (Button) findViewById(R.id.btn5);
        button5.setOnClickListener(this);
        button6 = (Button) findViewById(R.id.btn6);
        button6.setOnClickListener(this);
        button7 = (Button) findViewById(R.id.btn7);
        button7.setOnClickListener(this);
        button8 = (Button) findViewById(R.id.btn8);
        button8.setOnClickListener(this);
        button9 = (Button) findViewById(R.id.btn9);
        button9.setOnClickListener(this);
        buttonDelete = (Button) findViewById(R.id.btnDelete);
        buttonDelete.setOnClickListener(this);
        buttonEnter = (Button) findViewById(R.id.btnEnter);
        buttonEnter.setOnClickListener(this);

        keyValue.put(R.id.btn1, "1");
        keyValue.put(R.id.btn2, "2");
        keyValue.put(R.id.btn3, "3");
        keyValue.put(R.id.btn4, "4");
        keyValue.put(R.id.btn5, "5");
        keyValue.put(R.id.btn6, "6");
        keyValue.put(R.id.btn7, "7");
        keyValue.put(R.id.btn8, "8");
        keyValue.put(R.id.btn9, "9");
        keyValue.put(R.id.btn0, "0");
        keyValue.put(R.id.btnDelete, "\n");
    }

    @Override
    public void onClick(View v) {
        if (inputConnection == null)
            return;

        if (v.getId() == R.id.btnDelete) {
            CharSequence selectedText = inputConnection.getSelectedText(0);

            if (TextUtils.isEmpty(selectedText)) {
                inputConnection.deleteSurroundingText(1, 0);
            } else {
                inputConnection.commitText("", 1);
            }
        } else {
            String value = keyValue.get(v.getId());
            inputConnection.commitText(value, 1);
        }

    }

    public void setInputConnection(InputConnection ic) {
        inputConnection = ic;
    }
}
